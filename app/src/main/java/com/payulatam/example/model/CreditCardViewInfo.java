package com.payulatam.example.model;

public class CreditCardViewInfo {

  /** Credit card pan */
  private String pan;

  /** Credit card franchise */
  private CreditCardType type;

  private boolean askForCVV;

  /**
   * @param pan
   * @param type
   * @param askForCVV
   */
  public CreditCardViewInfo(String pan, CreditCardType type, boolean askForCVV) {
    this.pan = pan;
    this.type = type;
    this.askForCVV = askForCVV;
  }

  /**
   * @param pan
   * @param type
   */
  public CreditCardViewInfo(String pan, CreditCardType type) {
    this.pan = pan;
    this.type = type;
    this.askForCVV = false;
  }

  /**
   * @return the pan
   */
  public String getPan() {
    return pan;
  }

  @Override public String toString() {
    return "CreditCardViewInfo [pan=" + pan + ", type=" + type + ", asksForCVV=" + askForCVV + "]";
  }

  /**
   * @return the type
   */
  public CreditCardType getType() {
    return type;
  }
}
