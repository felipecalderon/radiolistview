package com.payulatam.example;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.payulatam.example.adapter.PureListViewAdapter;
import com.payulatam.example.model.CreditCardType;
import com.payulatam.example.model.CreditCardViewInfo;
import java.util.ArrayList;
import java.util.List;

public class PureListViewActivity extends BaseActivity implements AdapterView.OnItemClickListener {

  List<CreditCardViewInfo> cards = new ArrayList<>();
  PureListViewAdapter adapter;
  private ListView listView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    listView = (ListView) findViewById(R.id.list);
    populateData();
    adapter = new PureListViewAdapter(PureListViewActivity.this);
    adapter.notifyDataSetChanged(cards);
    listView.setAdapter(adapter);
    listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    listView.setOnItemClickListener(this);
    if (savedInstanceState == null) {
      selectFirstItem();
    }
  }

  @Override protected int getLayoutResource() {
    return R.layout.activity_pure_list_view;
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_pure_list_view, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {

    int id = item.getItemId();

    return id == R.id.action_settings || super.onOptionsItemSelected(item);
  }

  @Override public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
    CreditCardViewInfo creditCardViewInfoSelected = (CreditCardViewInfo) adapter.getItem(i);
    String pan = creditCardViewInfoSelected.getPan();
    String type = creditCardViewInfoSelected.getType().name();
    String message = String.format("Credit Card selected %s %s", pan, type);
    Toast.makeText(PureListViewActivity.this, message, Toast.LENGTH_SHORT).show();
  }

  private void populateData() {
    cards.add(new CreditCardViewInfo("****0808", CreditCardType.VISA, true));
    cards.add(new CreditCardViewInfo("****1206", CreditCardType.MASTERCARD, true));
    cards.add(new CreditCardViewInfo("****7912", CreditCardType.AMEX, false));
    cards.add(new CreditCardViewInfo("****5636", CreditCardType.DINERS, true));
  }

  private void selectFirstItem() {
    final int sFirsItem = 0;
    View v = adapter.getView(sFirsItem, null, null);
    long id = adapter.getItemId(sFirsItem);
    listView.performItemClick(v, sFirsItem, id);
  }
}
