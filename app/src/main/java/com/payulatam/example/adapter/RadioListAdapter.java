package com.payulatam.example.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import com.payulatam.example.R;
import com.payulatam.example.model.CreditCardViewInfo;
import java.util.ArrayList;
import java.util.List;

public class RadioListAdapter extends BaseAdapter {

  private ArrayList<CreditCardViewInfo> cardViewInfo = new ArrayList<>();
  private final LayoutInflater mInflater;

  public RadioListAdapter(Context context) {
    mInflater = LayoutInflater.from(context);
  }

  public void notifyDataSetChanged(List<CreditCardViewInfo> menus) {
    this.cardViewInfo.clear();
    this.cardViewInfo.addAll(menus);
    super.notifyDataSetChanged();
  }

  @Override public int getCount() {
    return cardViewInfo.size();
  }

  @Override public Object getItem(int position) {
    return cardViewInfo.get(position);
  }

  @Override public long getItemId(int position) {
    return position;
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {

    View view;
    RadioButton radioButton;

    if (convertView == null) {
      view = mInflater.inflate(R.layout.radio_list_row, parent, false);
    } else {
      view = convertView;
    }

    radioButton = (RadioButton) view;
    radioButton.setText(cardViewInfo.get(position).getPan());
    return radioButton;
  }
}