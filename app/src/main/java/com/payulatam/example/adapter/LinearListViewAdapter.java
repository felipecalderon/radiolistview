package com.payulatam.example.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.payulatam.example.R;
import com.payulatam.example.model.CreditCardViewInfo;
import java.util.ArrayList;
import java.util.List;

import static util.Utils.getCreditCardDrawableResourceLogo;

public class LinearListViewAdapter extends BaseAdapter {

  private ArrayList<CreditCardViewInfo> cardViewInfo = new ArrayList<>();
  private final LayoutInflater mInflater;

  public LinearListViewAdapter(Context context) {
    this.mInflater = LayoutInflater.from(context);
  }

  public void notifyDataSetChanged(List<CreditCardViewInfo> creditCardViewInfo) {
    this.cardViewInfo.clear();
    this.cardViewInfo.addAll(creditCardViewInfo);
    super.notifyDataSetChanged();
  }

  @Override public int getCount() {
    return cardViewInfo.size();
  }

  @Override public Object getItem(int position) {
    return cardViewInfo.get(position);
  }

  @Override public long getItemId(int position) {
    return position;
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {

    ViewHolder viewHolder;
    if (convertView == null) {
      viewHolder = new ViewHolder();
      convertView = mInflater.inflate(R.layout.linear_list_view_row, parent, false);
      viewHolder.cardLogo = (ImageView) convertView.findViewById(R.id.creditCardLogo);
      viewHolder.pan = (TextView) convertView.findViewById(R.id.maskedCardNumber);
      convertView.setTag(viewHolder);
    } else {
      viewHolder = (ViewHolder) convertView.getTag();
    }

    CreditCardViewInfo creditCardViewInfo = cardViewInfo.get(position);
    int resLogo = getCreditCardDrawableResourceLogo(creditCardViewInfo.getType());
    viewHolder.cardLogo.setImageResource(resLogo);
    viewHolder.pan.setText(creditCardViewInfo.getPan());
    return convertView;
  }

  private static class ViewHolder {
    ImageView cardLogo;
    TextView pan;
  }
}
