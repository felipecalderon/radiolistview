package com.payulatam.example;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.payulatam.example.adapter.LinearListViewAdapter;
import com.payulatam.example.model.CreditCardType;
import com.payulatam.example.model.CreditCardViewInfo;
import com.payulatam.radiolistview.widget.LinearListView;
import java.util.ArrayList;
import java.util.List;

import static com.payulatam.radiolistview.widget.LinearListView.*;

public class LinearListViewActivity extends BaseActivity
    implements OnItemClickListener {

  List<CreditCardViewInfo> cards = new ArrayList<>();
  private LinearListViewAdapter adapter;
  private LinearListView listView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    listView = (LinearListView) findViewById(R.id.list);
    populateData();
    adapter = new LinearListViewAdapter(LinearListViewActivity.this);
    adapter.notifyDataSetChanged(cards);
    listView.setAdapter(adapter);
    listView.setChoiceMode(CHOICE_MODE_SINGLE);
    listView.setOnItemClickListener(this);

    if (savedInstanceState == null) {
     selectFirstItem();
    }
  }

  @Override protected int getLayoutResource() {
    return R.layout.activity_linear_list_view;
  }

  private void populateData() {
    cards.add(new CreditCardViewInfo("****0808", CreditCardType.VISA, true));
    cards.add(new CreditCardViewInfo("****1206", CreditCardType.MASTERCARD, true));
    cards.add(new CreditCardViewInfo("****7912", CreditCardType.AMEX, false));
    cards.add(new CreditCardViewInfo("****5636", CreditCardType.DINERS, true));
  }

  @Override public void onItemClick(LinearListView parent, View view, int position, long id) {
    CreditCardViewInfo creditCardViewInfoSelected = (CreditCardViewInfo) adapter.getItem(position);
    String pan = creditCardViewInfoSelected.getPan();
    String type = creditCardViewInfoSelected.getType().name();
    String message = String.format("Credit Card selected %s %s", pan, type);
    Toast.makeText(LinearListViewActivity.this, message, Toast.LENGTH_SHORT).show();
  }

  private void selectFirstItem() {
    final int sFirsItem = 0;
    View v = adapter.getView(sFirsItem, null, null);
    long id = adapter.getItemId(sFirsItem);
    listView.performItemClick(v, sFirsItem, id);
  }
}
