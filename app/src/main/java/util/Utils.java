package util;

import com.payulatam.example.R;
import com.payulatam.example.model.CreditCardType;

public final class Utils {

  private Utils() {

  }

  public static int getCreditCardDrawableResourceLogo(CreditCardType creditCardType) {

    switch (creditCardType) {
      case VISA:
        return R.drawable.visa;

      case AMEX:
        return R.drawable.amex;

      case MASTERCARD:
        return R.drawable.master_card;

      case DINERS:
        return R.drawable.dinersclub;
      default:
        return R.drawable.unknown_cc;
    }
  }
}
