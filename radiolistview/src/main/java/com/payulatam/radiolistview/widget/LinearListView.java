package com.payulatam.radiolistview.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.LongSparseArray;
import android.util.SparseBooleanArray;
import android.view.SoundEffectConstants;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Checkable;
import android.widget.ListAdapter;
import com.payulatam.radiolistview.R;

/**
 * An extension of a linear layout that supports the divider API of Android
 * 4.0+. You can populate this layout with data that comes from a
 * {@link android.widget.ListAdapter}
 */
public class LinearListView extends LinearLayoutCompat {

  /**
   * Normal list that does not indicate choices
   */
  public static final int CHOICE_MODE_NONE = 0;

  /**
   * The list allows up to one choice
   */
  public static final int CHOICE_MODE_SINGLE = 1;

  /**
   * Controls if/how the user may choose/check items in the list
   */
  int mChoiceMode = CHOICE_MODE_NONE;

  /**
   * Running count of how many items are currently checked
   */
  int mCheckedItemCount;

  /**
   * Running state of which positions are currently checked
   */
  SparseBooleanArray mCheckStates;

  /**
   * Running state of which IDs are currently checked.
   * If there is a value for a given key, the checked state for that ID is true
   * and the value holds the last known position in the adapter for that id.
   */
  LongSparseArray<Integer> mCheckedIdStates;

  /**
   * The saved state that we will be restoring from when we next sync.
   * Kept here so that if we happen to be asked to save our state before
   * the sync happens, we can return this existing data rather than losing
   * it.
   */
  private SavedState mPendingSync;

  int mFirstPosition = 0;

  private static final int[] R_styleable_LinearListView = new int[] {
    /* 0 */android.R.attr.entries,
    /* 1 */R.attr.dividerThickness
  };

  private static final int LinearListView_entries = 0;
  private static final int LinearListView_dividerThickness = 1;
  private View mEmptyView;
  private ListAdapter mAdapter;
  private boolean mAreAllItemsSelectable;
  private OnItemClickListener mOnItemClickListener;
  private DataSetObserver mDataObserver = new DataSetObserver() {

    @Override public void onChanged() {
      setupChildren();
    }

    @Override public void onInvalidated() {
      setupChildren();
    }
  };

  public LinearListView(Context context) {
    this(context, null);
  }

  public LinearListView(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray a = context.obtainStyledAttributes(attrs, R_styleable_LinearListView);

    // Use the thickness specified, zero being the default
    final int thickness = a.getDimensionPixelSize(LinearListView_dividerThickness, 0);
    if (thickness != 0) {
      setDividerThickness(thickness);
    }

    CharSequence[] entries = a.getTextArray(LinearListView_entries);
    if (entries != null) {
      setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, entries));
    }

    a.recycle();
  }

  @Override public void setOrientation(int orientation) {
    if (orientation != getOrientation()) {
      int tmp = mDividerHeight;
      mDividerHeight = mDividerWidth;
      mDividerWidth = tmp;
    }
    super.setOrientation(orientation);
  }

  /**
   * Set the divider thickness size in pixel. That means setting the divider
   * height if the layout has an HORIZONTAL orientation and setting the
   * divider width otherwise.
   *
   * @param thickness The divider thickness in pixel.
   */
  public void setDividerThickness(int thickness) {
    if (getOrientation() == VERTICAL) {
      mDividerHeight = thickness;
    } else {
      mDividerWidth = thickness;
    }
    requestLayout();
  }

  public ListAdapter getAdapter() {
    return mAdapter;
  }

  /**
   * Sets the data behind this LinearListView.
   *
   * @param adapter The ListAdapter which is responsible for maintaining the data
   * backing this list and for producing a view to represent an
   * item in that data set.
   * @see #getAdapter()
   */
  public void setAdapter(ListAdapter adapter) {
    if (mAdapter != null) {
      mAdapter.unregisterDataSetObserver(mDataObserver);
    }

    mAdapter = adapter;

    if (mAdapter != null) {
      mAdapter.registerDataSetObserver(mDataObserver);
      mAreAllItemsSelectable = mAdapter.areAllItemsEnabled();
    }

    if (mChoiceMode != CHOICE_MODE_NONE && mCheckedIdStates == null) {
      mCheckedIdStates = new LongSparseArray<>();
    }

    if (mCheckStates != null) {
      mCheckStates.clear();
    }

    if (mCheckedIdStates != null) {
      mCheckedIdStates.clear();
    }

    setupChildren();
  }

  /**
   * Interface definition for a callback to be invoked when an item in this
   * LinearListView has been clicked.
   */
  public interface OnItemClickListener {

    /**
     * Callback method to be invoked when an item in this LinearListView has
     * been clicked.
     * <p>
     * Implementers can call getItemAtPosition(position) if they need to
     * access the data associated with the selected item.
     *
     * @param parent The LinearListView where the click happened.
     * @param view The view within the LinearListView that was clicked (this
     * will be a view provided by the adapter)
     * @param position The position of the view in the adapter.
     * @param id The row id of the item that was clicked.
     */
    void onItemClick(LinearListView parent, View view, int position, long id);
  }

  /**
   * Register a callback to be invoked when an item in this LinearListView has
   * been clicked.
   *
   * @param listener The callback that will be invoked.
   */
  public void setOnItemClickListener(OnItemClickListener listener) {
    mOnItemClickListener = listener;
  }

  /**
   * @return The callback to be invoked with an item in this LinearListView has
   * been clicked, or null id no callback has been set.
   */
  public final OnItemClickListener getOnItemClickListener() {
    return mOnItemClickListener;
  }

  /**
   * Call the OnItemClickListener, if it is defined.
   *
   * @param view The view within the LinearListView that was clicked.
   * @param position The position of the view in the adapter.
   * @param id The row id of the item that was clicked.
   * @return True if there was an assigned OnItemClickListener that was
   * called, false otherwise is returned.
   */
  public boolean performItemClick(View view, int position, long id) {

    if (mChoiceMode != CHOICE_MODE_NONE) {
      boolean checkedStateChanged = false;
      if (mChoiceMode == CHOICE_MODE_SINGLE) {
        boolean checked = !mCheckStates.get(position, false);
        if (checked) {
          mCheckStates.clear();
          mCheckStates.put(position, true);
          if (mCheckedIdStates != null && mAdapter.hasStableIds()) {
            mCheckedIdStates.clear();
            mCheckedIdStates.put(mAdapter.getItemId(position), position);
          }
          mCheckedItemCount = 1;
        } else if (mCheckStates.size() == 0 || !mCheckStates.valueAt(0)) {
          mCheckedItemCount = 0;
        }
        checkedStateChanged = true;
      }

      if (checkedStateChanged) {
        updateOnScreenCheckedViews();
      }
    }

    if (mOnItemClickListener != null) {
      playSoundEffect(SoundEffectConstants.CLICK);
      mOnItemClickListener.onItemClick(this, view, position, id);
      return true;
    }

    return false;
  }

  /**
   * Perform a quick, in-place update of the checked or activated state
   * on all visible item views. This should only be called when a valid
   * choice mode is active.
   */
  private void updateOnScreenCheckedViews() {
    final int firstPos = mFirstPosition;
    final int count = getChildCount();
    final boolean useActivated = getContext().getApplicationInfo().targetSdkVersion
        >= android.os.Build.VERSION_CODES.HONEYCOMB;
    for (int i = 0; i < count; i++) {
      final View child = getChildAt(i);
      final int position = firstPos + i;

      if (child instanceof Checkable) {
        ((Checkable) child).setChecked(mCheckStates.get(position));
      } else if (useActivated) {
        child.setActivated(mCheckStates.get(position));
      }
    }
  }

  public void setChoiceMode(int choiceMode) {
    mChoiceMode = choiceMode;
    if (mChoiceMode != CHOICE_MODE_NONE) {
      if (mCheckStates == null) {
        mCheckStates = new SparseBooleanArray(0);
      }
      if (mCheckedIdStates == null && mAdapter != null && mAdapter.hasStableIds()) {
        mCheckedIdStates = new LongSparseArray<>(0);
      }
    }
  }

  /**
   * Sets the view to show if the adapter is empty
   */
  public void setEmptyView(View emptyView) {
    mEmptyView = emptyView;

    final ListAdapter adapter = getAdapter();
    final boolean empty = ((adapter == null) || adapter.isEmpty());
    updateEmptyStatus(empty);
  }

  /**
   * When the current adapter is empty, the LinearListView can display a special
   * view call the empty view. The empty view is used to provide feedback to
   * the user that no data is available in this LinearListView.
   *
   * @return The view to show if the adapter is empty.
   */
  public View getEmptyView() {
    return mEmptyView;
  }

  /**
   * Update the status of the list based on the empty parameter. If empty is
   * true and we have an empty view, display it. In all the other cases, make
   * sure that the layout is VISIBLE and that the empty view is GONE (if
   * it's not null).
   */
  private void updateEmptyStatus(boolean empty) {
    if (empty) {
      if (mEmptyView != null) {
        mEmptyView.setVisibility(View.VISIBLE);
        setVisibility(View.GONE);
      } else {
        // If the caller just removed our empty view, make sure the list
        // view is visible
        setVisibility(View.VISIBLE);
      }
    } else {
      if (mEmptyView != null) mEmptyView.setVisibility(View.GONE);
      setVisibility(View.VISIBLE);
    }
  }

  private void setupChildren() {

    removeAllViews();

    updateEmptyStatus((mAdapter == null) || mAdapter.isEmpty());

    if (mAdapter == null) {
      return;
    }

    for (int i = 0; i < mAdapter.getCount(); i++) {
      View child = mAdapter.getView(i, null, this);
      if (mAreAllItemsSelectable || mAdapter.isEnabled(i)) {
        child.setOnClickListener(new InternalOnClickListener(i));
      }
      addViewInLayout(child, -1, child.getLayoutParams(), true);
    }
  }

  /**
   * Internal OnClickListener that this view associate of each of its children
   * so that they can respond to OnItemClick listener's events. Avoid setting
   * an OnClickListener manually. If you need it you can wrap the child in a
   * simple {@link android.widget.FrameLayout}.
   */
  private class InternalOnClickListener implements View.OnClickListener {

    int mPosition;

    public InternalOnClickListener(int position) {
      mPosition = position;
    }

    @Override public void onClick(View v) {
      if ((mAdapter != null)) {
        performItemClick(v, mPosition, mAdapter.getItemId(mPosition));
      }
    }
  }

  @Override protected Parcelable onSaveInstanceState() {
    Parcelable superState = super.onSaveInstanceState();

    SavedState ss = new SavedState(superState);

    if (mPendingSync != null) {
      ss.checkState = mPendingSync.checkState;
      ss.checkIdState = mPendingSync.checkIdState;
      ss.choiceMode = mPendingSync.choiceMode;
      return ss;
    }

    if (mCheckStates != null) {
      ss.checkState = mCheckStates.clone();
    }
    if (mCheckedIdStates != null) {
      final LongSparseArray<Integer> idState = new LongSparseArray<>();
      final int count = mCheckedIdStates.size();
      for (int i = 0; i < count; i++) {
        idState.put(mCheckedIdStates.keyAt(i), mCheckedIdStates.valueAt(i));
      }
      ss.checkIdState = idState;
    }

    if (mChoiceMode != CHOICE_MODE_NONE) {
      ss.choiceMode = mChoiceMode;
    }

    return ss;
  }

  @Override protected void onRestoreInstanceState(Parcelable state) {
    SavedState ss = (SavedState) state;
    super.onRestoreInstanceState(ss.getSuperState());
    mPendingSync = ss;

    if (ss.checkState != null) {
      mCheckStates = ss.checkState;
    }

    if (ss.checkIdState != null) {
      mCheckedIdStates = ss.checkIdState;
    }

    if (ss.choiceMode != CHOICE_MODE_NONE) {
      mChoiceMode = ss.choiceMode;
    }

    if (mChoiceMode != CHOICE_MODE_NONE) {
      updateOnScreenCheckedViews();
    }
  }

  static class SavedState extends BaseSavedState {
    SparseBooleanArray checkState;
    LongSparseArray<Integer> checkIdState;
    int choiceMode;

    /**
     * Constructor called from {@link LinearListView#onSaveInstanceState()}
     */
    SavedState(Parcelable superState) {
      super(superState);
    }

    /**
     * Constructor called from {@link #CREATOR}
     */
    private SavedState(Parcel in) {
      super(in);
      choiceMode = in.readInt();
      checkState = in.readSparseBooleanArray();
      final int N = in.readInt();
      if (N > 0) {
        checkIdState = new LongSparseArray<>();
        for (int i = 0; i < N; i++) {
          final long key = in.readLong();
          final int value = in.readInt();
          checkIdState.put(key, value);
        }
      }
    }

    @Override public void writeToParcel(Parcel out, int flags) {
      super.writeToParcel(out, flags);
      out.writeInt(choiceMode);
      out.writeSparseBooleanArray(checkState);
      final int N = checkIdState != null ? checkIdState.size() : 0;
      out.writeInt(N);
      for (int i = 0; i < N; i++) {
        out.writeLong(checkIdState.keyAt(i));
        out.writeInt(checkIdState.valueAt(i));
      }
    }

    public static final Parcelable.Creator<SavedState> CREATOR =
        new Parcelable.Creator<SavedState>() {
          @Override public SavedState createFromParcel(Parcel in) {
            return new SavedState(in);
          }

          @Override public SavedState[] newArray(int size) {
            return new SavedState[size];
          }
        };
  }
}
