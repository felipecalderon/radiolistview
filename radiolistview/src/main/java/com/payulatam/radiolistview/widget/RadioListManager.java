package com.payulatam.radiolistview.widget;

import android.database.DataSetObserver;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;

class RadioListManager extends DataSetObserver {

  private BaseAdapter adapter;
  private RadioGroup radioGroup;
  private static final int FIRST_ITEM = 0;

  RadioListManager(BaseAdapter adapter, RadioGroup group) {

    this.adapter = adapter;
    this.radioGroup = group;
  }

  @Override public void onChanged() {
    super.onChanged();
    addView();
  }

  private void addView() {
    for (int i = 0; i < adapter.getCount(); i++) {
      View view = adapter.getView(i, null, null);
      view.setId(i);
      if (view instanceof RadioButton) {
        radioGroup.addView(view);
      }
    }
  }

  void checkItem(int index) {
    if (index < adapter.getCount() || index > adapter.getCount()) {
      ((RadioButton) radioGroup.getChildAt(index)).setChecked(true);
    }
  }

  void selectFirstItem() {
    checkItem(FIRST_ITEM);
  }
}