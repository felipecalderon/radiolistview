package com.payulatam.radiolistview.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class RadioGroupListView extends LinearLayout {

  private RadioGroup mRadioGroup;

  private OnCheckedChangeListener checkedChangeListener;

  private RadioListManager radioListManager;

  private BaseAdapter adapter;

  public RadioGroupListView(Context context) {
    super(context);
    init();
  }

  public RadioGroupListView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public BaseAdapter getAdapter() {
    return adapter;
  }

  public void setAdapter(BaseAdapter adapter) {
    this.adapter = adapter;
    radioListManager = new RadioListManager(adapter, mRadioGroup);
    this.adapter.registerDataSetObserver(radioListManager);
  }

  private void init() {
    mRadioGroup = new RadioGroup(getContext());
    RadioGroup.LayoutParams params =
        new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT);
    mRadioGroup.setOrientation(LinearLayout.VERTICAL);
    mRadioGroup.setGravity(Gravity.START);
    mRadioGroup.setLayoutParams(params);

    addView(mRadioGroup);

  }

  @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
  }

  public OnCheckedChangeListener getCheckedChangeListener() {
    return checkedChangeListener;
  }

  public void selectFirstItem() {
    radioListManager.selectFirstItem();
  }

  public void setCheckedChangeListener(OnCheckedChangeListener checkedChangeListener) {
    this.checkedChangeListener = checkedChangeListener;
    mRadioGroup.setOnCheckedChangeListener(checkedChangeListener);
  }
}
